# T9 Phone App

I started the exericse initially to learn react, redux and the current JavaScript development workflow.

After getting the minimal requirements implemented in vanilla JavaScript, React and Redux/React, I fleshed out the converter to emulate the real T9 texting experience, first in vanilla JavaScript and then with the intention of porting it to React and then Redux.

Unfortunately I ran out of time to do the porting, but I think the implementation ended up being pretty neat (although not yet pretty), which you can see [here](http://edume-vanilla.s3-website-eu-west-1.amazonaws.com/).

(Try texting out some of the lyrics to Carly Rae Jepsen's Call Me Maybe!)

## Building

In root directory:

* `npm install`
* `sass --watch scss:css`

In `vanilla`, `react`, `redux` and `test`, run `build.sh` to invoke the required bundling/testing.

## Development

* Browserify (later Watchify) for developing with modules, with the Babelify (and React/JSX) tranformers applied
* Sass
* Mocha for tests
* ES6 language features (some)

## Files and Directory listings

### devlog.md

Loose development log and comments.

### issues.org

Issue tracking, categorised into bugs, tech debt, workflow, features and architecture.

### lib

Common functionality shared amongst `vanilla`, `react`, `redux` and `test`.

* `t9_core`: basic numbers to wordlist functions
* `t9_expansions` implemention (constructing a suffix trie and traversing it) for filtering words and word completions

### vanilla

Passable T9 emulation. Revamped the minimal implemention into an MVC architecture (an ode to the JS frameworks of yore!).

### react

Minimal implementation of a word list converter in react.

### redux

Minimal implementation of a word list converter in react/redux.

### misc

Quick and dirty script to generate a word list from a body of text (reads from and writes to `assets`).

### test

Tests for the modules in `lib`.

## Resources

Links I found useful for the new tech encountered:

* [Browserify](http://browserify.org/): Alternative to Bower.js; 'bundle' up require modules to be served up in the browser
* [ReactJS API](https://facebook.github.io/react/docs/top-level-api.html)
* [Lifecycle Methods](https://facebook.github.io/react/docs/component-specs.html)
* [Babelify presets](http://babeljs.io/docs/plugins/#presets): Set to ES2015 to access the bulk of 
* [Redux](https://github.com/reactjs/redux)
* [Redux docs](http://redux.js.org/docs/introduction/index.html)
* [Redux example](https://github.com/jackielii/simplest-redux-example)
* [Redux todo](http://redux.js.org/docs/basics/Actions.html)
* [ES6 Tour](http://jamesknelson.com/es6-the-bits-youll-actually-use/)
* [PropTypes discussion](http://wecodetheweb.com/2015/06/02/why-react-proptypes-are-important/)
* [PropTypes documentation](https://facebook.github.io/react/docs/reusable-components.html)
* [Reuseable components](https://facebook.github.io/react/docs/reusable-components.html): move to this by storing the state with Redux
