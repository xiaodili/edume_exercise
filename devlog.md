# EduMe exercise

Notes taken from the EduMe coding exercise

## React

## Resources

* [Browserify](http://browserify.org/): Alternative to Bower.js; 'bundle' up require modules to be served up in the browser
* [ReactJS API](https://facebook.github.io/react/docs/top-level-api.html)
* [Lifecycle Methods](https://facebook.github.io/react/docs/component-specs.html)
* [Babelify presets](http://babeljs.io/docs/plugins/#presets): Set to ES2015 to access the bulk of 
* [Redux](https://github.com/reactjs/redux)
* [Redux docs](http://redux.js.org/docs/introduction/index.html)
* [Redux example](https://github.com/jackielii/simplest-redux-example)
* [Redux todo](http://redux.js.org/docs/basics/Actions.html)
* [ES6 Tour](http://jamesknelson.com/es6-the-bits-youll-actually-use/)
* [PropTypes discussion](http://wecodetheweb.com/2015/06/02/why-react-proptypes-are-important/)
* [PropTypes documentation](https://facebook.github.io/react/docs/reusable-components.html)
* [Reuseable components](https://facebook.github.io/react/docs/reusable-components.html): move to this by storing the state with Redux

## Glossary

* **entry**: definition

# devlog

## 15/10/16

* **09:08**: Core function can be straightforwardly encapsulated - let's call it `numbersToWords`. Sticking it into a module... will figure out how browserify works later to get onto the client.
* **09:18**: Not really sure what 1 key should do... let make it return zero characters for now.
* **09:21**: Is there a cartesian product function in any of the JS helper libraries? Checking underscore and lodash...
* **09:25**: Aww I can't find anything. Found a relevant SO post though: http://stackoverflow.com/questions/12303989/cartesian-product-of-multiple-arrays-in-javascript
* **09:27**: Alright, using that one. Let's get node up to finish it off.

@begin=js@
var numbersToWords = function(numbers) {
    var letters = numbers.map(function(number) {
        return numberToWords[number];
    });
    var words = cartesianProduct(letters);
    return words.map(function(word) {
        return word.join('');
    });
};

numbersToWords([2,3]);
//[ 'ad', 'ae', 'af', 'bd', 'be', 'bf', 'cd', 'ce', 'cf' ]
@end=js@

* **09:40** Great, seems to work. Let's get zencoding installed again and figure out how to get that module onto the frontend with browserify.

Shall we use a `bin` and `src` directory...? Nah, hold off for now...

    pushd ~/dev/edume_exercise/vanilla
    npm install -g browserify
    browserify t9.js -o bundle.js
    ls
    bundle.js       index.html      t9.js

* **10:19** Cool, got it. Let's see if the `index.html` works. Works fine. Commit now, do the html next.

Let's just do a textbox first.

* **10:41**: I wonder how easy it would be to replicate some of knockout's data bindings. Let's timebox it for an hour.

Tightening feedback loop with a build script:

    #!/bin/bash
    # from http://stackoverflow.com/questions/59895/can-a-bash-script-tell-which-directory-it-is-stored-in
    DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
    pushd DIR && browserify mvc.js t9.js -o bundle.js

    chmod u+x build.sh

* **12:32**: Well, that took a lot longer than expected; turns out it's harder than expected to get that sweet sweet declarative binding working - gaining appreciation as to why more people don't roll their own MVCs. I think this is fine as PoC. What else do I want to do:

* error handling
* integrate node
* move to es6
* filter using a wordlist
* move to react
* move to react/redux
* nice UI

Reordering. Porting to node will slow down feedback cycle the most; do this last:

* filter using a wordlist (should be fun!)
* move to react
* move to react/redux
* move to es6
* error handling
* nice UI
* integrate node

Node interpreter for developing wordlist:

@begin=js@
var words = ["apples", "bananas", "cats", "dogs", "monkeys"];
var trie = createSuffixTrie(words);

isValidSuffix("app", trie);
//true
isValidSuffix("ape", trie);
//false
isValidSuffix("cats", trie);
//true
isValidSuffix("doggs", trie);
//false

var prefix_words = ["aes", "app", "cat", "dac", "mjn", "mon"];
filterWords(prefix_words, trie);
//[ 'app', 'cat', 'mon' ]
@end=js@

* **12:58**: Seems to work. Going to integrate it into the app, then get some food.

* **14:55**: Wow that was a nice break. Okay getting back into it.

* **15:19**: Filtering on words seems to work. Getting it to work with React now... let's start a new subdirectory...

Wait, let's do a bit more tidying up first so we can refactor the core logic into...

    7225243
    package

* **15:55**: Seems to work still!

    npm info react

Let's use 15.3.x then.

    dirs -c
    pushd ~/dev/edume_exercise/react
    npm install

Great, let's get a hello world up. Following this guide...

https://facebook.github.io/react/docs/getting-started.html#quick-start-without-npm

* **16:30**: Cool, got a workflow for working through those react examples in JavaScript: https://facebook.github.io/react/. Going to get the t9 functionality done, then add in JSX into the build step. Their todo app looks relevant to what we need...

Looking up what arguments createElement actually takes...

    ReactElement createElement(
      string/ReactClass type,
      [object props],
      [children ...]
    )

So the following line:

    ReactDOM.render(React.createElement(T9App, null), mountNode);

has

* type → T9App
* props → null
* No children

What about render?

    render(
        ReactElement element,
        DOMElement container,
        [function callback]
    )

Make sense. Great. What's going on 

Hmm I wonder if I should use JSX? Haha there is a course on learning React in 4 weeks. Don't think so. Although I do need a good workflow for getting all these React components up.

Well, even the timer example works, even though it's got ES6 stuff in it. Should probably still put a ES6 transpiler in the build chain somewhere.

* **17:03**: Cool, let's get it rendered down.

* **17:25**: Not sure why I have to pass in this value attribute... oh never mind, I think it just means that the value gets reflected too!

* **17:52**: That's feature parity on the vanilla JavaScript one. Pretty neat library, I guess. Let's convert from jsx with babelify to finish off the workflow, since that takes care of es6 as well.

Ooh... interesting read: http://babeljs.io/docs/plugins/#preset. Not much included in es2016 and es2017... let's just stick with es2015 then.

    npm install --save-dev babelify
    npm install --save-dev babel-preset-es2015 babel-preset-react

Great. Will test this by converting react code into JSX. Hmm... doesn't seem to like the init function. According to this, `var`-less variable declarations are now scoped under `window`? 

* **20:11**: Nice dinner. Okay let's read up on Redux. Oh my God it reads so contrived. Okay let's just try to get a basic example up and running

    npm install --save redux
    npm install --save react-redux

* **21:08**: That cool, I didn't know you could [destructure objects](https://developer.mozilla.org/en/docs/Web/JavaScript/Reference/Operators/Destructuring_assignment)

What does this `connect` function do? [Ah okay](https://github.com/reactjs/react-redux/blob/master/docs/api.md#connectmapstatetoprops-mapdispatchtoprops-mergeprops-options)

Wow, I didn't know you could do this in es6 as well:

    > text = "asd";
    'asd'
    > {text}
    { text: 'asd' }

* **00:17**: God, chewing through this redux example is taking forever. I think I'm finally on the right track, though!

* **00:31**: Right, let's move to Watchify to speed up React feedback cycles. Ooh... [watchify](https://github.com/substack/watchify) looks good!

    npm install -g watchify

Alright, let's just watch it on the other screen then. Wow okay... takes 3 seconds to compile apparently. Pretty interesting!

* **00:45**: Well, that was a bit annoying... stupid [combineReducers](http://stackoverflow.com/questions/35402389/combinereducers-causes-code-to-break)

* **08:54**: Woohoo, finally feature parity for vanilla, react and redux. Let's change it up so that it actually resembles a phone!

Hmm.. might get a testing framework up to make sure my expansions work properly. Mocha looks useable.

Other stuff:

* Watchify; taking a couple of seconds to compile!

Workflow:

Jesus, the amount of dependent technologies and tools needed to get this workflow going is craazy

* **16:12**: Righto, going to start working on the UI for a bit.

* **19:12**: Looking promising! Vanilla one almost done

* React
* Redux
* ES6
* Babel
* Watchify
* Browserify
* Sass
* Mocha
* Heroku


796247664933
synchronized

Before optimising app.js tests are running in ~700ms, and generateFilteredExpansions for `synchronized` doesn't run in under 2s. Optimising...

    var trie = expansions.createSuffixTrie(expansions.default_word_list);

    generateFilteredExpansions([7,9,6,2,4,7,6,6,4,9]);

Cool, runs in 16ms now! 

## 17/10/16

* **23:29**: Alright, this word list extraction script will be the last thing I do before typing up some documention, I promise!

* **00:20**: Documentation time. Heroku after.

    brew install heroku-toolbelt

    heroku login

Too much faffing about; going to test it on my machine, and then copy the prebuilt stuff into another directory and git that one.

Honestly, I'm not sure how this is going to work on Heroku. Might just stick it into S3 first... http://docs.aws.amazon.com/AmazonS3/latest/dev/HostingWebsiteOnS3Setup.html

Cool, works following that guide
