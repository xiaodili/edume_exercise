#!/usr/bin/python

import sys, getopt


def textToWords(text):
    legal_words = re.findall(r"[\w']+", text.lower().replace("'", ""))
    word_list = sorted(list(set(legal_words)))
    return word_list

def generateWords(infile, outfile):
    with open(infile, 'r') as f:
        text = f.read()
    word_list = textToWords(text)
    with open(outfile, 'w') as f:
        f.write('[' + ','.join(map(lambda word: '"' + word + '"', word_list)) + ']')


def main(argv):
   try:
       opts, args = getopt.getopt(argv,"hi:o:",["ifile=","ofile="])
   except getopt.GetoptError:
       print 'word_list_generator.py -i <inputfile> -o <outputfile>'
      sys.exit(2)
   for opt, arg in opts:
       if opt == '-h':
           print 'test.py -i <inputfile> -o <outputfile>'
         sys.exit()
     elif opt in ("-i", "--ifile"):
         inputfile = arg
     elif opt in ("-o", "--ofile"):
         outputfile = arg

if __name__ == "__main__":
    main(sys.argv[1:])
