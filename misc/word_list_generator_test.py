infile = "assets/call_me_maybe.txt"

with open(infile, 'r') as f:
    text = f.read()

text
# Out[4]: "I threw a wish in the well\nDon't ask me I'll never tell\nI looked at you as it fell\nAnd now you're in my way\n\nI trade my soul for a wish\nPennies and dimes for a kiss\nI wasn't looking for this\nBut now you're in my way\n\nYour stare was holding\nRipped jeans\nSkin was showing\nHot night\nWind was blowing\nWhere you think you're going baby?\n\nHey I just met you\nAnd this is crazy\nBut here's my number\nSo call me maybe\nIt's hard to look right at you baby\nBut here's my number\nSo call me maybe\n\nHey I just met you\nAnd this is crazy\nBut here's my number\nSo call me maybe\nAnd all the other boys\nTry to chase me\nBut here's my number\nSo call me maybe\n\nYou took your time with the call\nI took no time with the fall\nYou gave me nothing at all\nBut still you're in my way\n\nI beg and borrow and steal\nAt first sight and it's real\nI didn't know I would feel it\nBut it's in my way\n\nYour stare was holding\nRipped jeans\nSkin was showing\nHot night\nWind was blowing\nWhere you think you're going baby?\n\nHey I just met you\nAnd this is crazy\nBut here's my number\nSo call me maybe\nIt's hard to look right at you baby\nBut here's my number\nSo call me maybe\n\nBefore you came into my life\nI missed you so bad\nI missed you so bad\nI missed you so so bad\nBefore you came into my life\nI missed you so bad\nAnd you should know that\nI missed you so so bad, bad, bad, bad....\n\nIt's hard to look right at you baby\nBut here's my number\nSo call me maybe\n\nHey I just met you\nAnd this is crazy\nBut here's my number\nSo call me maybe\nAnd all the other boys\nTry to chase me\nBut here's my number\nSo call me maybe\n\nBefore you came into my life\nI missed you so bad\nI missed you so bad\nI missed you so so bad\nBefore you came into my life\nI missed you so bad\nAnd you should know that\nSo call me, maybe\n"

import re

text.lower().replace("'", "")
# Out[9]: 'i threw a wish in the well\ndont ask me ill never tell\ni looked at you as it fell\nand now youre in my way\n\ni trade my soul for a wish\npennies and dimes for a kiss\ni wasnt looking for this\nbut now youre in my way\n\nyour stare was holding\nripped jeans\nskin was showing\nhot night\nwind was blowing\nwhere you think youre going baby?\n\nhey i just met you\nand this is crazy\nbut heres my number\nso call me maybe\nits hard to look right at you baby\nbut heres my number\nso call me maybe\n\nhey i just met you\nand this is crazy\nbut heres my number\nso call me maybe\nand all the other boys\ntry to chase me\nbut heres my number\nso call me maybe\n\nyou took your time with the call\ni took no time with the fall\nyou gave me nothing at all\nbut still youre in my way\n\ni beg and borrow and steal\nat first sight and its real\ni didnt know i would feel it\nbut its in my way\n\nyour stare was holding\nripped jeans\nskin was showing\nhot night\nwind was blowing\nwhere you think youre going baby?\n\nhey i just met you\nand this is crazy\nbut heres my number\nso call me maybe\nits hard to look right at you baby\nbut heres my number\nso call me maybe\n\nbefore you came into my life\ni missed you so bad\ni missed you so bad\ni missed you so so bad\nbefore you came into my life\ni missed you so bad\nand you should know that\ni missed you so so bad, bad, bad, bad....\n\nits hard to look right at you baby\nbut heres my number\nso call me maybe\n\nhey i just met you\nand this is crazy\nbut heres my number\nso call me maybe\nand all the other boys\ntry to chase me\nbut heres my number\nso call me maybe\n\nbefore you came into my life\ni missed you so bad\ni missed you so bad\ni missed you so so bad\nbefore you came into my life\ni missed you so bad\nand you should know that\nso call me, maybe\n'

legal_words = re.findall(r"[\w']+", text.lower().replace("'", ""))

legal_words

len(legal_words)
# 379

# I wonder how many words are duplicated...
len(set(legal_words))
# 96. Not bad Carly!

word_list = sorted(list(set(legal_words)))

word_list

infile = "assets/call_me_maybe.txt"
outfile = "assets/call_me_maybe_wordlist.txt"
generateWords(infile, outfile)
