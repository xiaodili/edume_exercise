var React = require('react');
var ReactDOM = require('react-dom');
var T9 = require('../lib/t9_app.js');

//string → [int]
var updateNumbers = function(numbers_text) {
    return numbers_text.split('').map(function(num) {
        return parseInt(num);
    });
};

class T9App extends React.Component {
    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.t9 = props.t9;
        this.state = {
            numbers: [],
            words: [],
            text: ''
        };
    }
    handleChange(e) {
        this.setState({
            text: e.target.value,
            numbers: updateNumbers(e.target.value)
        });
    }
    handleSubmit(e) {
        e.preventDefault();
        this.setState({
            words: this.t9.generateFilteredExpansions(this.state.numbers)
        });
    }
    render() {
        return (
            <div>
                <h1>T9 Word List Generator</h1>
                <form onSubmit={this.handleSubmit}>
                    Numbers: <input onChange={this.handleChange} value={this.state.text}/>
                    <button>Submit</button>
                </form>
                <T9Words words={this.state.words} />
            </div>
        );
    }
}

class T9Words extends React.Component {
    render() {
        return (
            <ul>
                {this.props.words.map(word => (
                    <li key={word}>{word}</li>
                ))}
            </ul>
        );
    }
}

window.init = function() {
    var mountNode = document.getElementById('app');
    var t9 = new T9.app();
    ReactDOM.render(React.createElement(T9App, {t9}), mountNode);
}
