var assert = require('assert');

describe('App', function() {
    var app = new require('../lib/t9_app.js').app();
    describe('#generateFilteredExpansions()', function() {
        it('7225243 should be "package"', function() {
            assert.deepEqual(['package'], app.generateFilteredExpansions([7,2,2,5,2,4,3]));
        });
        //doesn't run yet; too slow!
        it('796247664933 should be "synchronized"', function() {
            assert.deepEqual(['synchronized'], app.generateFilteredExpansions([7,9,6,2,4,7,6,6,4,9,3,3]));
        });
    });
    describe('#seekLastWord()', function() {
        var word_boundaries = ['.', ' '];
        it('should return the starting index of the word"', function() {
            assert.deepEqual(["Something", 4], app.seekLastWord("Hi. Something", word_boundaries));
        });
        it('edge condition for start of text', function() {
            assert.deepEqual(["Something", 0], app.seekLastWord("Something", word_boundaries));
        });
    });
});
