var assert = require('assert');

describe('Expansions', function() {
    var expansion = require('../lib/t9_expansions.js');
    var trie = expansion.createSuffixTrie(expansion.default_word_list);
    describe('#getCompletions()', function() {
        it('should return the correct expansions for default word list', function() {
            assert.deepEqual(["extends"], expansion.getCompletions(["ext"], trie));
            //onmouseover, onmouseup, onmousedown, onmouseout, onmousemove
            assert.equal(5, expansion.getCompletions(["onmouse"], trie).length);
            assert.deepEqual([], expansion.getCompletions(["asd"], trie));
        });
    });
});

describe('Completions', function() {
    var expansion = require('../lib/t9_expansions.js');
    var trie = expansion.createSuffixTrie(['i', 'is', 'ice']);
    describe('#getWordCompletion()', function() {
        it('should return the correct completions', function() {
            assert.deepEqual(3, expansion.getCompletions(["i"], trie).length);
        });
    });
});
