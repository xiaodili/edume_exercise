var assert = require('assert');

describe('Core', function() {
    var core = require('../lib/t9_core.js');
    describe('#expandNumber()', function() {
        it('should expand current ', function() {
            assert.deepEqual(['aa', 'ab', 'ac', 'ba', 'bb', 'bc'], core.expandNumber(['a','b'], 2));
        });
    });
});
