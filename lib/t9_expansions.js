//Flatten an array, one level deep: http://stackoverflow.com/a/10865042/1010076
var flattenOnce = function(xss) {
    return [].concat.apply([], xss);
};

// [string] → trie
var createSuffixTrie = function(words) {
    var words_with_termination = words.map(function(word) {
        return word + '$';
    });
    var root = {};
    for (var i = 0; i < words_with_termination.length; i++) {
        var word = words_with_termination[i];
        var current_node = root;
        for (var j = 0; j < word.length; j++) {
            var letter = word[j];
            if (!current_node.hasOwnProperty(letter)) {
                current_node[letter] = {};
            }
            current_node = current_node[letter];
        }
    }
    return root;
};

// string → bool
var isValidPrefix = function(word, trie) {
    var current_node = trie;
    for (var i = 0; i < word.length; i++) {
        var letter = word[i];
        if (current_node.hasOwnProperty(letter)) {
            current_node = current_node[letter];
        } else {
            return false;
        }
    }
    return true;
};

//[string], trie → [string]
var filterWords = function(words, trie) { //move this into a curry?
    return words.filter(function(word) {
        return isValidPrefix(word, trie);
    })
};

//string → [string]
var getWordCompletion = function(prefix, trie) {
    //traverse to prefix of trie
    var current_node = trie;
    for (var i = 0; i < prefix.length; i++) {
        var letter = prefix[i];
            current_node = current_node[letter];
            //word should really be a guaranteed prefix, due to having called filterWords first
            if (current_node === undefined) {
                return [];
            }
    }
    //[[node, string]]
    var visiting_stack = [[current_node, '']];
    var suffices = [];
    while (visiting_stack.length > 0) {
        var node, suffix;
        [node, suffix] = visiting_stack.pop();
        if (Object.keys(node).length == 0) {
            suffices.push(suffix.slice(0, -1));
        }
        for (var key in node) {
            var next_node = node[key];
            visiting_stack.push([next_node, suffix + key]);
        }
    }
    return suffices.map(function(suffix) {
        return prefix + suffix;
    });
};

//[string] → [string]
var getCompletions = function(words, trie) {
    return flattenOnce(words.map(function(word) {
        return getWordCompletion(word, trie);
    })).sort(function(a, b) {
        return a.length - b.length;
    });

};

//
//var default_word_list = ["abstract", "else", "instanceof", "super", "boolean", "enum", "int", "switch", "break", "export", "interface", "synchronized", "byte", "extends", "let", "this", "case", "false", "long", "throw", "catch", "final", "native", "throws", "char", "finally", "new", "transient", "class", "float", "null", "true", "const", "for", "package", "try", "continue", "function", "private", "typeof", "debugger", "goto", "protected", "var", "default", "if", "public", "void", "delete", "implements", "return", "volatile", "do", "import", "short", "while", "double", "in", "static", "with", "alert", "frames", "outerHeight", "all", "frameRate", "outerWidth", "anchor", "function", "packages", "anchors", "getClass", "pageXOffset", "area", "hasOwnProperty", "pageYOffset", "Array", "hidden", "parent", "assign", "history", "parseFloat", "blur", "image", "parseInt", "button", "images", "password", "checkbox", "Infinity", "pkcs11", "clearInterval", "isFinite", "plugin", "clearTimeout", "isNaN", "prompt", "clientInformation", "isPrototypeOf", "propertyIsEnum", "close", "java", "prototype", "closed", "JavaArray", "radio", "confirm", "JavaClass", "reset", "constructor", "JavaObject", "screenX", "crypto", "JavaPackage", "screenY", "Date", "innerHeight", "scroll", "decodeURI", "innerWidth", "secure", "decodeURIComponent", "layer", "select", "defaultStatus", "layers", "self", "document", "length", "setInterval", "element", "link", "setTimeout", "elements", "location", "status", "embed", "Math", "String", "embeds", "mimeTypes", "submit", "encodeURI", "name", "taint", "encodeURIComponent", "NaN", "text", "escape", "navigate", "textarea", "eval", "navigator", "top", "event", "Number", "toString", "fileUpload", "Object", "undefined", "focus", "offscreenBuffering", "unescape", "form", "open", "untaint", "forms", "opener", "valueOf", "frame", "option", "window", "onbeforeunload", "ondragdrop", "onkeyup", "onmouseover", "onblur", "onerror", "onload", "onmouseup", "ondragdrop", "onfocus", "onmousedown", "onreset", "onclick", "onkeydown", "onmousemove", "onsubmit", "oncontextmenu", "onkeypress", "onmouseout", "onunload"];

// use javascript reserved word list as default (lowercase)
var default_word_list = ["abstract", "else", "instanceof", "super", "boolean", "enum", "int", "switch", "break", "export", "interface", "synchronized", "byte", "extends", "let", "this", "case", "false", "long", "throw", "catch", "final", "native", "throws", "char", "finally", "new", "transient", "class", "float", "null", "true", "const", "for", "package", "try", "continue", "function", "private", "typeof", "debugger", "goto", "protected", "var", "default", "if", "public", "void", "delete", "implements", "return", "volatile", "do", "import", "short", "while", "double", "in", "static", "with", "alert", "frames", "outerheight", "all", "framerate", "outerwidth", "anchor", "function", "packages", "anchors", "getclass", "pagexoffset", "area", "hasownproperty", "pageyoffset", "array", "hidden", "parent", "assign", "history", "parsefloat", "blur", "image", "parseint", "button", "images", "password", "checkbox", "infinity", "pkcs11", "clearinterval", "isfinite", "plugin", "cleartimeout", "isnan", "prompt", "clientinformation", "isprototypeof", "propertyisenum", "close", "java", "prototype", "closed", "javaarray", "radio", "confirm", "javaclass", "reset", "constructor", "javaobject", "screenx", "crypto", "javapackage", "screeny", "date", "innerheight", "scroll", "decodeuri", "innerwidth", "secure", "decodeuricomponent", "layer", "select", "defaultstatus", "layers", "self", "document", "length", "setinterval", "element", "link", "settimeout", "elements", "location", "status", "embed", "math", "string", "embeds", "mimetypes", "submit", "encodeuri", "name", "taint", "encodeuricomponent", "nan", "text", "escape", "navigate", "textarea", "eval", "navigator", "top", "event", "number", "tostring", "fileupload", "object", "undefined", "focus", "offscreenbuffering", "unescape", "form", "open", "untaint", "forms", "opener", "valueof", "frame", "option", "window", "onbeforeunload", "ondragdrop", "onkeyup", "onmouseover", "onblur", "onerror", "onload", "onmouseup", "ondragdrop", "onfocus", "onmousedown", "onreset", "onclick", "onkeydown", "onmousemove", "onsubmit", "oncontextmenu", "onkeypress", "onmouseout", "onunload"];

//[string], trie → [string]
exports.filterWords = filterWords;
//[string] → [string]
exports.getCompletions = getCompletions;
// [string] → trie
exports.createSuffixTrie = createSuffixTrie;
// [string]
exports.default_word_list = default_word_list;
