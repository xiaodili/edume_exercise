var core = require('./t9_core.js');
var expansions = require('./t9_expansions.js');

var T9_app = function(words = expansions.default_word_list) {
    var trie = expansions.createSuffixTrie(words);

    //[int] → string
    var generateOneExpansion = function(numbers) {
        return core.numbersToFirstExpansion(numbers);
    }
    //[int] → [string]
    var generateExpansions = function(numbers) {
        return core.numbersToAllExpansions(numbers);
    }
    //[int] → [string]
    var generateFilteredExpansions = function(numbers) {
        if (numbers.length == 0) {
            return [];
        }
        const [head, ...rest] = numbers;
        var words = core.numberToWords[head];
        for (var i = 0; i < rest.length; i++) {
            var number = rest[i];
            words = core.expandNumber(words, number);
            words = expansions.filterWords(words, trie);
        }
        return words;
    }
    //[string] → [string]
    var generateCompletions = function(words) {
        return expansions.getCompletions(words, trie);
    };
    //string → [int]
    var generateModelNumbers = function(word) {
        return core.wordToNumbers(word);
    };

    //string → [string, int]
    var seekLastWord = function(text, word_boundaries) {
        var pointer = text.length - 1;
        while (word_boundaries.indexOf(text[pointer]) == -1 && pointer >= 0) {
            pointer -= 1;
        }
        return [text.slice(pointer + 1), pointer + 1];
    };
    return {
        generateExpansions, //currently not being used
        generateOneExpansion,
        generateFilteredExpansions,
        generateCompletions,
        generateModelNumbers,
        seekLastWord
    }
}

exports.app = T9_app;
