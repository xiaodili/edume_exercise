//Flatten an array, one level deep: http://stackoverflow.com/a/10865042/1010076
var flattenOnce = function(xss) {
    return [].concat.apply([], xss);
};

//From http://stackoverflow.com/a/29585704/1010076
var cartesianProduct = function(a) { // a = array of array
    var i, j, l, m, a1, o = [];
    if (!a || a.length == 0) return a;

    a1 = a.splice(0, 1)[0]; // the first array of a
    a = cartesianProduct(a);
    for (i = 0, l = a1.length; i < l; i++) {
        if (a && a.length) for (j = 0, m = a.length; j < m; j++)
            o.push([a1[i]].concat(a[j]));
        else
            o.push([a1[i]]);
    }
    return o;
}

// int → [string]
var numberToWords = {
    2: ['a', 'b', 'c'],
    3: ['d', 'e', 'f'],
    4: ['g', 'h', 'i'],
    5: ['j', 'k', 'l'],
    6: ['m', 'n', 'o'],
    7: ['p', 'q', 'r', 's'],
    8: ['t', 'u', 'v'],
    9: ['w', 'x', 'y', 'z']
};

// char → int
var charToNumber = {
    'a': 2,
    'b': 2,
    'c': 2,
    'd': 3,
    'e': 3,
    'f': 3,
    'g': 4,
    'h': 4,
    'i': 4,
    'j': 5,
    'k': 5,
    'l': 5,
    'm': 6,
    'n': 6,
    'o': 6,
    'p': 7,
    'q': 7,
    'r': 7,
    's': 7,
    't': 8,
    'u': 8,
    'v': 8,
    'w': 9,
    'x': 9,
    'y': 9,
    'z': 9
}

// string → [int]
var wordToNumbers = function(word) {
    return word.split("").map(function(char) {
        return charToNumber[char];
    });
}

// [int] → [string]
var numbersToAllExpansions = function(numbers) {
    var letters = numbers.map(function(number) {
        return numberToWords[number];
    });
    var words = cartesianProduct(letters);
    return words.map(function(word) {
        return word.join('');
    });
};

// No lazy evaluation, so use this to pick up the first expansion generated
// [int] → string
var numbersToFirstExpansion = function(numbers) {
    return numbers.map(function(number) {
        return numberToWords[number][0];
    }).join('');
};

// for performance, when doing branching and bounding
// [string], int → [string]
var expandNumber = function(words, number) {
    return flattenOnce(words.map(function(word) {
        return numberToWords[number].map(function(letter) {
            return word + letter;
        });
    }));
}

// [int] → [string]
exports.numbersToAllExpansions = numbersToAllExpansions;
// string → [int]
exports.wordToNumbers = wordToNumbers;

//exported for performance
// [int] → string
exports.numbersToFirstExpansion = numbersToFirstExpansion;
// [string], int → [string]
exports.expandNumber = expandNumber;
// int → [string]
exports.numberToWords = numberToWords;
