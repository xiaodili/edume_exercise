#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

pushd $DIR && browserify app.js -o bundle.js -t [ babelify --presets [ es2015 react ] ] && popd;

# Run this in another process:
# watchify app.js -o bundle.js -t [ babelify --presets [ es2015 react ] ] -v
