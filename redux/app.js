import React, { Component, PropTypes } from 'react'
import ReactDOM from 'react-dom'
import { createStore, combineReducers } from 'redux'
import { Provider, connect } from 'react-redux'
import T9 from '../lib/t9_app.js'

var t9 = new T9.app();
//string → [int]
var updateNumbers = function(numbers_text) {
    return numbers_text.split('').map(function(num) {
        return parseInt(num);
    });
};

// React component
const T9App = ({submitPressed, textTyped, text, words}) => (
    <div>
        <h1>T9 Word List Generator</h1>
        <form onSubmit={e => {
            e.preventDefault()
            submitPressed(text)
        }}>
            Numbers:
            <input onChange={e => {
                textTyped(e.target.value)
            }} value={text}/>
            <button>Submit</button>
        </form>
        <T9Words words={words} />
    </div>
)

T9App.propTypes = {
    submitPressed: PropTypes.func.isRequired,
    textTyped: PropTypes.func.isRequired,
    text: PropTypes.string.isRequired
}

const T9Words = ({words}) => (
    <ul>
        {words.map(word => (
            <li key={word}>{word}</li>
        ))}
    </ul>
)

//Actions
const TEXT_TYPED = 'TEXT_TYPED'
const SUBMIT_PRESSED = 'SUBMIT_PRESSED'

//Action creators
const textTyped = (text) => {
    return {
        type: TEXT_TYPED,
        text
    }
}

const submitPressed = (text) => {
    return {
        type: SUBMIT_PRESSED,
        text
    }
}

const initialState = {
    text: '',
    words: []
}
function textReducer(state = initialState, action) {
    const text = state.text
    switch (action.type) {
        case TEXT_TYPED:
            return Object.assign({}, state, {
                text: action.text
            })
        case SUBMIT_PRESSED:
            const numbers = updateNumbers(state.text)
            const words = t9.generateFilteredExpansions(numbers)
            return Object.assign({}, state, {
                words
            })
        default:
            return state
    }
}

const t9Reducer = combineReducers({
    textReducer
});

// Store
const store = createStore(t9Reducer);

// Map Redux state to component props
function mapStateToProps(state) {
    return {
        text: state.textReducer.text,
        words: state.textReducer.words
    }
}

// Map Redux actions to component props
function mapDispatchToProps(dispatch) {
    return {
        submitPressed: () => {
            dispatch(submitPressed())
        },
        textTyped: (text) => {
            dispatch(textTyped(text))
        }
    }
}

const App = connect(
    mapStateToProps,
    mapDispatchToProps
)(T9App)

window.init = function() {
    ReactDOM.render(
        <Provider store={store}>
        <App />
        </Provider>,
        document.getElementById('app')
    )
}
