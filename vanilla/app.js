var T9 = require('../lib/t9_app.js');

var T9View = function(t9) {
    var resolved_text_ui, current_expansion_ui, current_completion_ui, expansions_ui;
    var generateExpansionHTML = function(word) {
        return "<li class='expansion'><span>" + word + "</span></li>";
    };

    //public
    var renderExpansions = function(words, selected_index) {
        expansions_ui.innerHTML = words.map(function(word) {
            return generateExpansionHTML(word);
        }).join('');
        var completions = expansions_ui.getElementsByClassName("expansion");
        if (completions.length > 0) {
            completions[selected_index].className += " expansion-selected";
        }
    };
    var renderAroundCursor = function(numbers, viable_expansions, completions, completion_index) {
        if (viable_expansions.length == 0) { //no expansions left, pick first gibberish word that's generated
            current_expansion_ui.innerHTML = t9.generateOneExpansion(numbers);
            current_completion_ui.innerHTML = "";
        } else if (completions.length == 0) { //no completions left but still expansions left; pick first expansion (i.e. just finished a recognised word)
            current_expansion_ui.innerHTML = viable_expansions[0];
            current_completion_ui.innerHTML = "";
        } else { //some completions left; pick the one selected
            var prefix_length = numbers.length;
            current_expansion_ui.innerHTML = completions[completion_index].slice(0, prefix_length);
            current_completion_ui.innerHTML = completions[completion_index].slice(prefix_length);
        }
    };
    var setUIElements = function(resolved_text, current_expansion, current_completion, expansions) {
        resolved_text_ui = resolved_text;
        current_expansion_ui = current_expansion;
        current_completion_ui = current_completion;
        expansions_ui = expansions;
    };
    var renderResolvedText = function(resolved_text) {
        resolved_text_ui.innerHTML = resolved_text.replace(/\s/g, '&nbsp;');
    };
    return {
        renderExpansions,
        renderAroundCursor,
        setUIElements,
        renderResolvedText
    };
};

var T9Model = function() {
    //string
    var resolved_text = "";
    //[int]
    var numbers = [];
    //int
    var autocomplete_index = 0;
    //[string]
    var expansions = [];
    //[string]
    var completions = [];
    return {resolved_text, numbers, expansions, completions, autocomplete_index};
};

var T9Controller = function(model, view, t9) {
    //internal
    var word_boundaries = ['.', ' '];
    var reCalculateExpansions = function() {
        model.expansions = t9.generateFilteredExpansions(model.numbers);
        model.completions = t9.generateCompletions(model.expansions);
    };
    var updateExpansionUI = function() {
        //view.renderExpansions(model.expansions, model.autocomplete_index);
        view.renderExpansions(model.completions, model.autocomplete_index); //looks nicer!
        view.renderAroundCursor(model.numbers, model.expansions, model.completions, model.autocomplete_index);
    };
    var updateResolvedTextUI = function() {
        view.renderResolvedText(model.resolved_text);
    };
    var clearExpansions = function() {
        model.expansions = [];
        model.completions = [];
    };
    var resolveExpansion = function() {
        if (model.expansions.length == 0) { //no expansions left, pick first gibberish word that's generated
            model.resolved_text += t9.generateOneExpansion(model.numbers);
        } else if (model.completions.length == 0) { //no completions left but still expansions left; pick first expansion (i.e. just finished a valid word)
            model.resolved_text += model.expansions[0];
        } else { //some completions left; pick first one
            model.resolved_text += model.completions[model.autocomplete_index];
        }
        model.numbers = [];
    };

    //methods
    var key1Pressed = function() {
        model.numbers.push(1);
        model.autocomplete_index = 0;
        reCalculateExpansions();
        updateExpansionUI();
    };
    var key2Pressed = function() {
        model.numbers.push(2);
        model.autocomplete_index = 0;
        reCalculateExpansions();
        updateExpansionUI();
    };
    var key3Pressed = function() {
        model.numbers.push(3);
        model.autocomplete_index = 0;
        reCalculateExpansions();
        updateExpansionUI();
    };
    var key4Pressed = function() {
        model.numbers.push(4);
        model.autocomplete_index = 0;
        reCalculateExpansions();
        updateExpansionUI();
    };
    var key5Pressed = function() {
        model.numbers.push(5);
        model.autocomplete_index = 0;
        reCalculateExpansions();
        updateExpansionUI();
    };
    var key6Pressed = function() {
        model.numbers.push(6);
        model.autocomplete_index = 0;
        reCalculateExpansions();
        updateExpansionUI();
    };
    var key7Pressed = function() {
        model.numbers.push(7);
        model.autocomplete_index = 0;
        reCalculateExpansions();
        updateExpansionUI();
    };
    var key8Pressed = function() {
        model.numbers.push(8);
        model.autocomplete_index = 0;
        reCalculateExpansions();
        updateExpansionUI();
    };
    var key9Pressed = function() {
        model.numbers.push(9);
        model.autocomplete_index = 0;
        reCalculateExpansions();
        updateExpansionUI();
    };
    var key0Pressed = function() {
        resolveExpansion();
        clearExpansions();
        model.resolved_text += " ";
        updateResolvedTextUI();
        model.autocomplete_index = 0;
        updateExpansionUI();
    };
    var key1Pressed = function() {
        model.autocomplete_index = 0;
        resolveExpansion();
        clearExpansions();
        model.resolved_text += ".";
        updateResolvedTextUI();
        model.autocomplete_index = 0;
        updateExpansionUI();
    };
    var keyDelPressed = function() {
        if (model.numbers.length == 0 && model.resolved_text.length == 0) {
            return;
        }

        model.autocomplete_index = 0;
        if (model.numbers.length > 0) { //middle of typing a word
            model.numbers.pop();
            reCalculateExpansions();
            updateExpansionUI();
        }
        else { 
            var last_char = model.resolved_text.slice(-1);
            if (word_boundaries.indexOf(last_char) !== -1) { // deleting a noncharacter
                model.resolved_text = model.resolved_text.slice(0, -1);
                updateResolvedTextUI();
            }
            else { // deleting through a recognised word
                var start_index, word;
                [word, start_index] = t9.seekLastWord(model.resolved_text, word_boundaries);
                model.numbers = t9.generateModelNumbers(word.slice(0, -1));
                model.resolved_text = model.resolved_text.slice(0, start_index);
                updateResolvedTextUI();
                reCalculateExpansions();
                updateExpansionUI();
            }
        }
    };
    var keyUpPressed = function() { //go up, i.e. DOWN in index numbers
        if (model.completions.length == 0) {
            return;
        }
        model.autocomplete_index = Math.max(model.autocomplete_index - 1, 0);
        updateExpansionUI();
    };
    var keyDownPressed = function() { //go down, i.e. UP in index numbers
        if (model.completions.length == 0) {
            return;
        }
        model.autocomplete_index = Math.min(model.autocomplete_index + 1, model.completions.length - 1);
        updateExpansionUI();
    };
    return {
        key0Pressed,
        key1Pressed,
        key2Pressed,
        key3Pressed,
        key4Pressed,
        key5Pressed,
        key6Pressed,
        key7Pressed,
        key8Pressed,
        key9Pressed,
        key0Pressed,
        keyDelPressed,
        keyUpPressed,
        keyDownPressed
    };
};

var applyT9Bindings = function(model, view, controller) {
    //e.g. "foreach: words" → "foreach"
    var getBindType = function(attr) {
        return attr.split(':')[0];
    };
    var bindTypes = {
        "resolved_text": function(elem) {
            resolved_text = elem;
        },
        "current_expansion": function(elem) {
            current_expansion = elem;
        },
        "current_completion": function(elem) {
            current_completion = elem;
        },
        "expansions": function(elem) {
            expansions = elem;
        },
        "key_1": function(elem) {
            elem.onclick = controller.key1Pressed;
        },
        "key_2": function(elem) {
            elem.onclick = controller.key2Pressed;
        },
        "key_3": function(elem) {
            elem.onclick = controller.key3Pressed;
        },
        "key_4": function(elem) {
            elem.onclick = controller.key4Pressed;
        },
        "key_5": function(elem) {
            elem.onclick = controller.key5Pressed;
        },
        "key_6": function(elem) {
            elem.onclick = controller.key6Pressed;
        },
        "key_7": function(elem) {
            elem.onclick = controller.key7Pressed;
        },
        "key_8": function(elem) {
            elem.onclick = controller.key8Pressed;
        },
        "key_9": function(elem) {
            elem.onclick = controller.key9Pressed;
        },
        "key_0": function(elem) {
            elem.onclick = controller.key0Pressed;
        },
        "key_del": function(elem) {
            elem.onclick = controller.keyDelPressed;
        },
        "key_up": function(elem) {
            elem.onclick = controller.keyUpPressed;
        },
        "key_down": function(elem) {
            elem.onclick = controller.keyDownPressed;
        },
        "key_tab": function(elem) {
            elem.onclick = controller.keyTabPressed;
        }
    };
    var resolved_text;
    var current_expansion;
    var current_completion;
    var expansions;
    var databinds = document.querySelectorAll('[data-t9]');
    for (var i = 0; i < databinds.length; i++) {
        var databind = databinds[i];
        var databind_attr = databind.getAttribute('data-t9');
        var bind_type = getBindType(databind_attr);
        bindTypes[bind_type](databind);
    }
    view.setUIElements(resolved_text, current_expansion, current_completion, expansions);

    //get some keyboard bindings up for the power users
    var keyboardBindings = {
        "ArrowUp": controller.keyUpPressed,
        "ArrowDown": controller.keyDownPressed,
        "k": controller.keyUpPressed,
        "j": controller.keyDownPressed,
        "Backspace": controller.keyDelPressed,
        "0": controller.key0Pressed,
        "1": controller.key1Pressed,
        "2": controller.key2Pressed,
        "3": controller.key3Pressed,
        "4": controller.key4Pressed,
        "5": controller.key5Pressed,
        "6": controller.key6Pressed,
        "7": controller.key7Pressed,
        "8": controller.key8Pressed,
        "9": controller.key9Pressed
    }
    document.onkeyup = function(e) {
        if (e.key in keyboardBindings) {
            keyboardBindings[e.key]();
        }
    };
};

var word_list = ["a","all","and","as","ask","at","baby","bad","before","beg","blowing","borrow","boys","but","call","came","chase","crazy","didnt","dimes","dont","fall","feel","fell","first","for","gave","going","hard","heres","hey","holding","hot","i","ill","in","into","is","it","its","jeans","just","kiss","know","life","look","looked","looking","maybe","me","met","missed","my","never","night","no","nothing","now","number","other","pennies","real","right","ripped","should","showing","sight","skin","so","soul","stare","steal","still","tell","that","the","think","this","threw","time","to","took","trade","try","was","wasnt","way","well","where","wind","wish","with","would","you","your","youre"];

window.init = function() {
    var t9 = new T9.app(word_list);
    var model = new T9Model();
    var view = new T9View(t9);
    var controller = new T9Controller(model, view, t9);
    applyT9Bindings(model, view, controller);
};
